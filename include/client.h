#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>

#ifndef CLIENT_H_
#define CLIENT_H_

typedef struct client_s client_t;
typedef struct s_client_request t_client_request;

#include <include/server.h>
#include <include/player.h>

struct client_s {
	int sock;
	struct sockaddr_in server;
	t_server_game *server_game;
};

struct s_client_request {
	unsigned int magic; /* client ID asignné opar le server */
	int x_pos;			/* x position desirer par un client */
	int y_pos;			/* y position desirer par client */
	int dir;			/* direction desirer par client */
	int command;		/* client command. 0 : do nothing, 1 : drop bomb */
	int speed;			/* vitesse du joueur */
	int checksum;		/* simple checksum */
};

t_server_game *global_game;

client_t *init_client(char *ip_addr, unsigned short port);

int send_client_data(client_t *client_data, player_t *player);
t_server_game *init_server_game(void);
t_server_game *receive_server_data(client_t *client_data);

int get_magic(client_t *client_struct);

void *client_listening(void *client_data);
#endif
