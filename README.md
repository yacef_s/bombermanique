
### Build
Dependencies : 
- libsdl2-dev
- libsdl2-image-dev
- libsdl2-ttf-dev

```bash
$ sudo apt install libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev
```

Pour build  : `make`


You can generate doc with : `make docs`.
Vous pouvez génerer la doc : `make docs`.

### Menu

<center><img src="/menu_demo.png"></center>

If you are running it in a VM there is an issue with unknown caracters.


### support
Tested on GNU/Linux Debian 9
